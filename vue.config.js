module.exports = {
  configureWebpack: {
    devtool: 'source-map'
  },
  crossorigin: 'anonymous',
  devServer: {
    host: 'localhost',
    port: 8080,
    hot: true,
    liveReload: true
  }
}
