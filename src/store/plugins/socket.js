import store from '../index'

export const connectToSocket = url => {
  let socket = new WebSocket(url)

  socket.onopen = function() {
    store.dispatch('Server/setConnectionStatus', 'Connected')
  }

  socket.onmessage = function(event) {
    store.dispatch('Server/SOCKET_getServerTime', JSON.parse(event.data))
  }

  socket.onclose = function() {
    store.dispatch('Server/setConnectionStatus', 'Disconnected')
    store.dispatch('Server/connect')
  }

  socket.onerror = function() {
    store.dispatch('Server/setConnectionStatus', 'Disconnected')
    store.dispatch('Server/connect')
  }
  return socket
}
