import axios from 'axios'
import config from '../../config'

const Axios = axios.create({
  baseURL: config.baseURL
})

let token = sessionStorage.getItem('token')
Axios.defaults.headers.common['x-test-app-jwt-token'] = token

export default Axios
