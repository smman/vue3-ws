import { createStore, createLogger } from 'vuex'
import Authorization from './modules/authorization/index.js'
import Server from './modules/server/index.js'
const debug = process.env.NODE_ENV !== 'production'

export default new createStore({
  modules: {
    Authorization,
    Server
  },
  devtools: true,
  strict: true,
  plugins: debug ? [createLogger()] : []
})
