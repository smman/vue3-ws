/* ============
 * State of the Authorization module
 * ============
 */

export default () => ({
  token: sessionStorage.getItem('token') || null
})
