/* ============
 * Actions for the Authorization module
 * ============
 */

import Axios from '../../plugins/axios'
import { AUTH_SUCCESS, LOGOUT } from './mutation-types'

export const login = ({ commit }, user) => {
  return new Promise((resolve, reject) => {
    Axios.post('/api/login', user)
      .then(res => {
        const token = res.headers['x-test-app-jwt-token']
        sessionStorage.setItem('token', token)
        Axios.defaults.headers.common['x-test-app-jwt-token'] = token
        commit(AUTH_SUCCESS, token)
        resolve()
      })
      .catch(error => {
        reject(error)
      })
  })
}

export const logout = ({ commit }) => {
  return new Promise(resolve => {
    commit(LOGOUT)
    sessionStorage.removeItem('token')
    delete Axios.defaults.headers.common['Authorization']
    resolve()
  })
}

export default {
  login,
  logout
}
