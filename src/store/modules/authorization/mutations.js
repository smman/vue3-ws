/* ============
 * Mutations for the Authorization module
 * ============
 */

import { AUTH_SUCCESS, LOGOUT } from './mutation-types'

export default {
  [AUTH_SUCCESS](state, token) {
    state.token = token
  },

  [LOGOUT](state) {
    state.token = null
  }
}
