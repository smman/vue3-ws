/* ============
 * Mutation types for the Authorization module
 * ============
 */

export const AUTH_SUCCESS = 'AUTH_SUCCESS'
export const LOGOUT = 'LOGOUT'

export default {
  AUTH_SUCCESS,
  LOGOUT
}
