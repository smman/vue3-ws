/* ============
 * Mutations for the Server module
 * ============
 */

import { SET_CONNECTION_STATUS, SOCKET_GET_SERVER_TIME } from './mutation-types'

export default {
  [SET_CONNECTION_STATUS](state, status) {
    state.connectionStatus = status
  },

  [SOCKET_GET_SERVER_TIME](state, time) {
    state.time = time
  }
}
