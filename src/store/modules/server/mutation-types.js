/* ============
 * Mutation types for the Server module
 * ============
 *
 */

export const SET_CONNECTION_STATUS = 'SET_CONNECTION_STATUS'
export const SOCKET_GET_SERVER_TIME = 'SOCKET_GET_SERVER_TIME'

export default {
  SET_CONNECTION_STATUS,
  SOCKET_GET_SERVER_TIME
}
