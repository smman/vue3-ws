/* ============
 * State of the Server module
 * ============
 */

export default {
  connectionStatus: 'Disconnected',
  time: null
}
