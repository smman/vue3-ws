/* ============
 * Actions for the Server module
 * ============
 */

import Axios from '../../plugins/axios'
import { SOCKET_GET_SERVER_TIME, SET_CONNECTION_STATUS } from './mutation-types'
import { connectToSocket } from '../../plugins/socket'

export const connect = () => {
  return new Promise((resolve, reject) => {
    Axios.get('/api/subscribe')
      .then(res => {
        window.socket = connectToSocket(res.data.url)
        resolve()
      })
      .catch(error => {
        reject(error)
      })
  })
}

export const setConnectionStatus = ({ commit }, status) => {
  commit(SET_CONNECTION_STATUS, status)
}

export const SOCKET_getServerTime = ({ commit }, payload) => {
  commit(SOCKET_GET_SERVER_TIME, payload.server_time)
}

export default {
  connect,
  setConnectionStatus,
  SOCKET_getServerTime
}
